Convert a gml graph file to svg image file with optional sugiyama layout using ogdf graph layout lib.

On Linux:
cd OGDF-snapshot-2018-03-28
cmake .
make
cd ..
qmake
make

example use:
gmltosvg -sugi u.gml u.svg
convert u.svg u.png

gmltosvg -help

using imagick tool 'convert -background none image.svg image.png'

This is GNU GPL version 3 libre software
