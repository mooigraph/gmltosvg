/* these files are GNU GPL version 3 libre software */

/* GNU GPL version 3 - do a sugiyama layout */

#include <ogdf/fileformats/GraphIO.h>
#include <ogdf/layered/MedianHeuristic.h>
#include <ogdf/layered/OptimalHierarchyLayout.h>
#include <ogdf/layered/OptimalRanking.h>
#include <ogdf/layered/SugiyamaLayout.h>

using namespace ogdf;

int main(int argc, char *argv[])
{
	bool dosugi=false;
	char *infilename;
	char *outfilename;
	Graph G;
	GraphAttributes GA(G,
	  GraphAttributes::nodeGraphics |
	  GraphAttributes::edgeGraphics |
	  GraphAttributes::nodeLabel |
	  GraphAttributes::edgeStyle |
	  GraphAttributes::nodeStyle |
	  GraphAttributes::nodeTemplate);

	// too few arguments passed?
	if(argc <= 2) {
		std::cout << "Call with: gmltosvg [ Options ] input.gml output.svg" << std::endl;
		std::cout << "Type \'gmltosvg -help\' for help" << std::endl;
		return 1;
	}

	// process all arguments
	for(int i = 1; i < argc; ++i)
	{
		if(strcmp(argv[i], "-help") == 0) {
			std::cout << "Call with: gmltosvg [ Options ] input.gml output.svg" << std::endl;
			std::cout << "\nwhere Options are:" << std::endl;
			std::cout << "  -sugi" << std::endl;
			std::cout << "     specifies to do a sugiyama layout" << std::endl;
			std::cout << "  -help" << std::endl;
			std::cout << "     displays this help and exits" << std::endl;
			return 0;
		} else if(strcmp(argv[i], "-sugi") == 0) {
			dosugi=true;
		} else {
			if(i+2 < argc) {
				std::cout << "too many arguments without options:" << argv[i] << std::endl;
				return 1;
			}
			infilename = argv[i];
			outfilename = argv[i+1];
			break;
		}
	}

	// std::cout << "infile=" << infilename << std::endl;
	// std::cout << "outfile=" << outfilename << std::endl;

	// should be svg name for output
	if (!outfilename) {
		std::cerr << "Missing output svg filename after input gml file " << infilename << std::endl;
		return 1;
	}

	if (!GraphIO::read(GA, G, infilename, GraphIO::readGML)) {
		std::cerr << "Could not parse input gml file " << infilename << std::endl;
		return 1;
	}

	// We assign a width and height of 10.0 to each node as minimum size
	for (node v : G.nodes)
	{
		if (GA.width(v) == 0 || GA.height(v) == 0) {
			GA.width(v) = GA.height(v) = 10.0;
		}
	}

	// sugiyama layout with option -sugi
	if (dosugi) {


	SugiyamaLayout SL;
	SL.setRanking(new OptimalRanking);
	SL.setCrossMin(new MedianHeuristic);

	OptimalHierarchyLayout *ohl = new OptimalHierarchyLayout;
	ohl->layerDistance(30.0);
	ohl->nodeDistance(25.0);
	ohl->weightBalancing(0.8);
	SL.setLayout(ohl);

	SL.call(GA);
	}

	GraphIO::write(GA, outfilename, GraphIO::drawSVG);

	return 0;
}

